# Cyanotype Prints

## Idea

Make good looking prints with harmless chemicals that look really cool. Maybe even use this process for photography.

## Recipe

There are different recipes found on the internet. Many will work but will provide different results. Over at https://simplifier.neocities.org/cyanotype.html a very thorough process description is given. Yet, I do not want to use Ferric Ammonium Oxalate over Ferric Ammonium Citrate as the process then looses its chemical benignity.

### Parts

A - 25 g of Ferric Ammonium Citrate in 100 ml of H2O
B - 10 g of Potassium Ferricyanide in 100 ml of H2O

Dissolve both salts in water and store the A solution in a dark space. Mix both parts in a one to one ratio just before applying it onto paper. For prints with a bit more contrast, a slight excess of solution A can be used.

## Apply onto Paper

The solution can be brushed onto paper but this leaves visible marks. If you want to avoid this, drops of the solution can be wiped evenly across the surface by a glass rod or credit card. This yields a more homogenous result. Let the paper dry completely before the next step.

## Exposing

Depending on the light source, the paper should be exposed from 30 Minutes (strong sun, directly from above) to multiple hours (overcast day). A good indicator that the paper is fully exposed is the colour: when the paper starts to turn from dark blue into brown, a good print can be achieved. 

## Washing

Wash the paper in a tray with water. Gently sway the paper until all yellow hue on the paper is gone. To brighten the paper and to increase contrast, add a bit of acid (vinegar).

## Drying

Let dry on flat surface ot by hanging.

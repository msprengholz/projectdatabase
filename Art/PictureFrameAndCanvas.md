# Picture Frame and Paper Canvas

## Idea

Make a simple picture frame out of wood and attach paper to it, so that it will be taught similar to a canvas frame (no wrinkles, taught surface that sounds like a drum when hit). The paper can be drawn on while it is attached to the frame or before.

## Picture Frame

Take wood you have at hand. I used a 30x10x2000 mm strip of pine from the hardware store. To cut one single picture frame (that fits my 450 mm width paper on a roll perfectly) from this strip you need the following parts cut:
```
       a
|------------|
|            |
|            |
|            | b
|            |
|            |
|------------|
```

a = 565 mm
b = 450 mm
U = 2*(a+b) = 2090 mm
t = front width of wood = 15 mm

Depending on how you construct your frame (45° or 90° angles in the corners) you need to adjust the length of each strip accordingly. For 45° corners (looks best imo) you cut the strip in the same sequence as you'd put it together later on. This ensures a somewhat good fit. But beware that each other strip has to be turned over. So check that your wood is pleasant to look at from both sides.

cutting order:

1: 595 mm - t/2;
2: 450 mm - t;
3: 595 mm - t;
4: 450 mm - t/2

At the ends you'll lose two corners (as the wood is square). Glue the cut strips together with wood glue.

## Gluing The Paper to The Frame

### Preparing The Frame

Make sure that the surface is not too rough. Either sand slightly or scrape smooth using a razor blade (or a plane if you have). Apply a watersoluble glue that does not leave any marks on the paper. I use pure methyl cellulose (wall paper glue). Alternatively, you may use dextrin (british gum) for a stronger bond. Mix the methyl cellulose (MC) with fewer water than is recommended to form a thick paste (around 1 part MC to 20 parts water by weight). This is then evenly applied to the frame.

### Wetting The Paper

The paper needs to be wet when it is glued to the frame. This ensures that when it dries it taughtens to form a level surface. Wetting is best done by hanging the paper from a few clips on a line and then gently misting it with water from a spray bottle. Alternatively, you may also squirt it wet with a toothbrush. The paper should expand but still handle easily without tearing. You will have to try this for each type of paper. It is also easiest if you have not yet painted on / printed the paper. Obviously water based colours will probably run during this step. It is therefore best to paint those after the paper is attached to the frame.

### Gluing Paper to Frame

Set the wet paper on the still wet glue from one side of the frame. Try to lay the paper straight down, step by step, until you have reached the opposite side of the frame. The paper can still be moved on the wet glue. You may make use of this to adjust the paper a bit. It is not important to completely prevent the paper from sagging at this step. Try to get it as evenly as possible. The drying of the paper will allow for quite a few wrinkles! Press the paper with some weight onto the frame and let dry. While the paper is still wet you can tear of any excess paper protruding over the frame. For cleaner edges use a razor blade. 

## Attaching the Frame to The Wall

I drilled two holes (0.5 mm diameter) in the top two corner "ears" and attached a string from on side to the other. This can then be hung from a nail. This method does not interfere with attachment of the paper which can therefore be removed if so wanted. 

## Common issues

- paper does not stick to the frame:
    - use more / thicker / stronger glue
    - the glue can additionally be worked into the back of the paper while it is already on the frame
    - increase pressing force
- paper is not taught in all areas
    - respray the paper with water; further drying will increase taughtness
    - use a bit methyl cellulose in water (don't thicken the consistensy considerably) to get an even stronger effect
- hole in paper
    - cut a strip of tissue paper (or other thin paper) that covers the hole and glue it on using methyl cellulose
    - the lighter the paper the less visible will the repair be

## Images

Front of the picture frame:

<img src="Pictures/PF_front.jpg" alt="drawing" width="200"/>

Back of the picture frame with taught paper surface:

<img src="Pictures/PF_back.jpg" alt="drawing" width="200"/>

Attachment of string at corner (Notice the not so perfect joint, but it still works!):

<img src="Pictures/PF_corner.jpg" alt="drawing" width="200"/>


## Bioplastik (Polyester)

### Idee

Biologisch abbaubares Plastik, das dennoch beständig gegen äußere Umwelteinflüsse ist. Es soll sowohl zum Kleben als auch für Faserverbunde eingesetzt werden können. Es handelt sich um ein duroplastisches Polyester. Es besteht aus Glycerin und Citronensäure und ggf. Zusätzen wie Salzsäure. Diese werden bei Temperaturen von 80 bis 140 °C reagiert. Je höher die Temperatur, umso schneller läuft die Reaktion ab (48 h bis wenige Minuten). Temperaturen über 100 °C sorgen durch den entstehenden Wasserdampf (einziges Beiprodukt der Reaktion) für ein Aufschäumen des Kunststoffs. So können sehr leichte Sandwichkerne erstellt werden. Über 140 °C zersetzt sich die Citronensäure (wird gelb bis dunkel braun). Dadurch entsteht ein extrem spröder Schaum, der nicht mehr gut klebt! Durch ändern des Verhältnisses zwischen Glycerin und Citronensäure können die Eigenschaften angepasst werden. Je mehr Glycerin anteilig vorhanden ist, umso duktiler verhält sich das Material. Es kann ein Memory-Schaum ähnlicher Zustand erreicht werden. Bei vollständiger Reaktion entweicht rund 30% der Reaktionsmasse als Wasserdampf. Die Glasübergangstemperatur liegt dann bei rund 80 °C. Der Polyester haftet hervorragend an organischen Stoffen (Holz) sowie an Glas und an Metall. Er ist komplett ungiftig und vollständig biologisch abbaubar.

### Reaktionsverhältnisse

|                     | Glycerin  | Citronensäure (monohydrat) | Citronensäure (rein) |
| ------------------- | --------- | -------------------------- | -------------------- |
| M in g/mol          | 92,09     | 210,14                     | 192,13               |
| molare Verhältnisse | 1 bis 1,2 | 1                          | 0,914                |

 ### Verbesserung

Die Glasübergangstemperatur und Reaktionsgeschwindigkeit kann durch den Zusatz von Salzsäure angeblich gesteigert werden. Die Wasserbeständigkeit muss getestet werden.

### Update

Nach 2 Jahren im Innenraum beginnt das Plastik sich zu zersetzen. Es wird weich und klebrig. Die Reaktion macht sich rückgängig.

### Quellen

- Alberts, Albert H., and Gadi Rothenberg. 2017. “Plantics-GX: A Biodegradable and Cost-Effective Thermoset Plastic That Is 100% Plant-Based.” *Faraday Discussions* 202: 111–20.
- Halpern, Jeffrey M. et al. 2014. “A Biodegradable Thermoset Polymer Made by Esterification of Citric Acid and Glycerol.” *Journal of biomedical materials research. Part A* 102(5): 1467–77.
- Berube, Marc-Andre et al. 2018. “Determination of In Situ Esterification Parameters of Citric Acid-Glycerol Based Polymers for Wood Impregnation.” *Journal of Polymers and the Environment* 26(3): 970–79.



TODO:
- add specimen pictures

# Gypsum / Plaster of Paris molds

## Idea

Use gypsum to create low cost, highly detailed molds or casts. Add additves to make the gypsum harder.

## Previous Experiments

I used special gypsum calcined at high temperature (~ 950°C). This variant is called "hochgebrannter Gips" (high temperature gypsum) or "totgebrannter Gips" ("inactive gypsum") because it hardens very slowly when mixed with water and no additives (up to a month). It should posses better mechanical properties (similar to concrete: higher compression strenght, lower water solubility than normal gypsum). Through the addition of potassium carbonate (K2CO3, 27.6 g per litre of water) it should harden within 24 h.

The result did not satisfy me. The surface was easily scratched and sanded of when rubbed with cloth. Thus it could not be polished. The material is also hard to get (not sold in Germany to regular people) and has a higher carbon footprint as it needs much higher temperatures than regular gypsum during manufacturing.

## Better Gypsum Recipes

I tried various additives and report the best results below. I used cheap, regular gypsum from the hardware store (Knauf Bau- und Elektrikergips). It did contain some unwanted additves as gas was produced by the addition of citric acid. This should not happen. Dextrin had the biggest influence: it made the gypsum harder and polishable (no more particles separating from the surface).

| Recipe | Ingredients | Results |
| ------ | ------ | ------ |
| 1 (reference) | 100 g gypsum powder, 40 g H2O, 1 drop of concentrated dish soap | good surface quality, not scratch resistant (finger nail test) |
| 2 | 100 g gypsum, 40 g H2O, 8 g Dextrin (added to dry powder) | gypsum hardens faster (around 1 minute pot life), smooth surface (can be polished to a light sheen), harder surface than fingernail, gray deposits on surface |
| 3 | like 2, but 50 g H2O instead of 40 g | gives more pot life, else roughly equal |
| 4 | 100 g gypsum, 50 g H2O, 8 g Dextrin, 2 g citric acid (monohydrate) | 30 minute pot life, many pores due to gas development |

Use less water to get harder products.

General procedure:
- mix dry powders (gypsum and additves)
- pour water in container
- add powders, let sit until soaked 
- then stir to homogenous paste

Additives:
- Dextrin (can be made from roasting starch in oven at 160°C, increase hardness and ductility)
- Dish soap (produces higher details on surface)
- Hair / fibres can be used for reinforcing the putty, if they absorb water wet them beforehand
- Citric acid (longer pot life, harder end product)

light weight pore gypsum:
- high water to powder ratio
- mix water with dish soap until foamy, them add gypsum powder and mix further (best with an electric stirrer)
- produces very light foams
- needs a long time to dry

## Polishing
Gypsum has many pores that should be closed to get a shiny surface

- linseed oil (at least 3 applications) produce a matte finish when buffed with a cloth
- rosin (colophony) solved in ethanol applied with a brush produces a sticky surface (2 applications)
  - when buffed produces a faint mirror like surface

## Molding

- dish soap is an excellent mold release

### Sources

- Mack, Ludwig. “Das sogenannte Alaunisieren des Gipses.” (http://storage.lib.uchicago.edu/pres/2013/pres2013-0019.pdf)
- Auras, Michael. 2010. “Der Werkstoff Gips.” *ICOMOS – Hefte des Deutschen Nationalkomitees* 50: 78–84.
- Kühn, Hermann. 1996. “Was ist Stuck?” *ICOMOS – Hefte des Deutschen Nationalkomitees* 19: 17–24. (All addites are thoroughly explained)

----------
first tests, german

## Gipsform

### Idee

Ein Formenbaumaterial sollte folgende Eigenschaften haben: nahezu schrumpflos aushärten, hohe Detailgenauigkeit, relativ harte Oberfläche, möglichst duktil. In diesem speziellen Fall kommt noch hinzu, dass Temperaturen bis 140 °C ausgehalten werden müssen. Unter solchen Anforderungen bleibt nur noch Gips als nachhaltige Option über. Es ist nicht duktil, kann aber mit Fasern verstärkt werden. Die Oberfläche ist nicht hart (1 auf der Mohs-Skala). Dies kann allerdings durch andere Zusätze verbessert werden.  Normaler Modellbaugips erweist sich als zu bröselig, selbst mit einigen Zusätzen (es wurden nicht alle möglichen getestet!). Anhydrit ist die 'totgebrannte' Form des Gipses, die nur noch extrem langsam Wasser aufnimmt und dadurch erst über mehrere Tage erhärtet. Dies kann über Zusätze beschleunigt werden. So ausgehärteter Anhydrit ist härter als Modellbaugips und kann zudem durch die Zugabe von Salzen (z.B. Kaliumcarbonat) an der Oberfläche Kalkstein (Mohshärte 3) bilden.

### Rezept

Auf 100 g Gips kommen 35 ml Wasser. Auf einen Liter Anrührwasser kommen 27,6 g Kaliumcarbonat (K2CO3). Die so angerührte Masse erhärtet nach einem Tag und trocknet nach wenigen Tagen vollständig durch. Sie ist deutlich abriebfester als Modellbaugips, lässt sich stellenweise aber dennoch mit dem Fingernagel einritzen. Die Oberfläche hat einen leichten Glanz und lässt sich polieren.

### Verbesserung

- einbringen von Faserverstärkungen (Juteband)
- Abformen durch Kalk/Graphit/Öl-Schlicke verbessern
- einlegen von Heizdrähten (Edelstahl)

### Quellen

- I can not find the source for the dextrin anymore. It was in some old german lexicon on zeno.org
- Mack, Ludwig. “Das sogenannte Alaunisieren des Gipses.” (http://storage.lib.uchicago.edu/pres/2013/pres2013-0019.pdf)
- Auras, Michael. 2010. “Der Werkstoff Gips.” *ICOMOS – Hefte des Deutschen Nationalkomitees* 50: 78–84.
- Kühn, Hermann. 1996. “Was ist Stuck?” *ICOMOS – Hefte des Deutschen Nationalkomitees* 19: 17–24. (Beschreibung vieler möglicher Zusätze)


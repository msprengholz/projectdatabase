## Leinöllack

### Idee

Eine Alternative zu 2K-Lack, der sich auf vielen Oberflächen anwenden lässt. Leinöl ist bekannt als Holzschutzmittel, lässt sich aber auch auf anderen Werkstoffen (Metalle, Keramiken, organische Verbindungen) anwenden. Durch eine Oxidationsreaktion mit Luftsauerstoff polymerisiert Leinöl zu Linoxin, einem gummiartigen, stark elastischen Material. Durch große erreichbare Dehnungen und Klebefähigkeit, hält es auch an sich verformenden Körpern gut. Es platzt nicht ab. Leinölfarben können überstrichen / überlackiert werden. Dabei gilt die Regel: von mager zu fett. Dies gilt sowohl für die Anzahl der Pigmente in der Farbe als auch für die Viskosität des Öls. 

Beispiel Holzschutz:  Grundierung mit kaltgepresstem Leinöl, dann mehrere Lagen gekochtes, abschließend dickes Standöl mit Farbpigmenten (weiß für beste Witterungsbeständigkeit)

Der unteren Ölschicht immer genug Zeit zum Trocknen lassen. Leinöl dehnt sich aus während der Oxidation. Bei Nicht-Beachtung kann es zur Absprengung der äußeren Schicht kommen!

| Leinölarten  | Eigenschaften                                                |
| :----------- | ------------------------------------------------------------ |
| kaltgepresst | niedrigviskos, zieht tief in Oberflächen ein, lange Härtezeit von mind. 7 Tagen (Umgebungstemperatur- und Luftzufuhrabhängig) |
| gekocht      | etwas zähflüssiger als kaltgepresstes Öl, zieht nicht so tief ein, kürzere Aushärtezeit |
| Leinölfirnis | oft Zusatz von gesundheitsbedinklichen Trockenstoffen, Aushärtung in 1-2 Tagen |
| Standöl      | kaltgepresste und gekochte Öle werden für mehrere Monate stehen gelassen (in geschlossenem Glas), um diese anzudicken. Dickere Öle sind witterungsbeständiger. |

  

### Anrühren von Ölfarbe

Leinöl wird mit mineralischen Farbpigmenten (Titanoxid, Eisenoxide) versetzt und verrührt. Dabei ist je nach Pigment das Verhältnis von Öl zu Pigmenten unterschiedlich. Für deckende Farben (also besonders bei dünnen Schichten!) müssen die Pigmente optimalerweise in das Öl gewalzt werden. Sind nicht alle Pigmente mit Öl benetzt, bröseln sie aus der Farbschicht wieder heraus. Für niedrigere Konzentrationen geht auch ein Rührstabaufsatz auf der Bohrmaschine.



### Sprühen von Ölfarbe

Es wurde weiße Ölfarbe (Titanoxid mit Leinölfirnis) angerührt. Das Gemisch war noch leicht gelblich und nicht viel viskoser als Wasser. Es wurde über eine Airbrushpistole an einem Kompressor auf mehrere Testflächen gesprüht (Papier, Holz, Glas, UP-Vorgelat-Lack). Die Farbe war nicht deckend genug für einen dünnen Auftrag. Durch große Schichtdicken dauerte das Aushärten eine Woche (komplett durchgehärtet, 18°C, Kellerraum). Die Farbe haftete auf allen Oberflächen und erzielte der Oberfläche entsprechende Oberflächenqualität. Auf Glasplatte härtete die Schicht glatt und glänzend zur Luftseite und matt und glatt zur Glasseite hin aus. Die Oberflächen sind nicht kratzresistent und kleben zu Beginn leicht.

Ölfarbe lässt sich am besten mit einem Stofflappen wegwischen (Achtung! Entzündungsgefahr). Aceton säubert auch mit Öl benetzte Oberflächen. Lösungsmittel für Ölfarbe sind Terpentin, Terpentinersatz (Testbenzin) und Orangenschalenöl.

#### Verbesserung

Für ein besseres Ergebnis sollte professionell angemischte Ölfarbe (gewalzt oder mit Glasläufer angerührt) mit Lösemittel (Terpentin, Terpentinersatz, Orangen-/Citrusschalenöl) verdünnt werden, um diese sprühen zu können. Damit sollten sich auch sehr dünne Schichten realisieren lassen. 

### Ölschliff

Eine gute, glatte Oberfläche ist extrem wichtig vor dem Lackieren. Mittels des Ölschliffes kann dies erreicht werden. Es wird mit Schleifpapier das Bauteil nass geschliffen, jedoch mit Öl anstatt mit Wasser. Dadurch werden kleinste Öffnungen geschlossen und es entsteht eine glatte, matt-glänzende Oberfläche. Leider sind auch die Sandkörner als graue Einschlüsse im Endergebnis zu sehen. Diese sollten aber unter der Lackierung verschwinden.




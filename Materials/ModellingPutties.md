## Modelling Putties

## Ideas 

Recipes for use in handshaping of forms.

## Wood (not paper) mache

Ingredients:
  - 1 l saw dust (from the local carpenter or your own shop)
  - 1/2 l of water
  - 31 g of wallpaper glue / paste (methyl cellulose)

Mix water and glue and let sit for atleast 20 Minutes. A stiff gel should form. Knead the saw dust into the glue to form a homeogenous dough. The mass is easily shaped. The high water content leads to considerable shrinkage and long drying times. Mold may develop if the drying takes too long. This can be avoided by additives or a different glue.

The so formed parts are very light and handle like wood. They can be sanded, drilled and sawed. For stronger parts long fibers could be incorporated and a stronger glue used (see soy protein glue). 

---------

## Modelliermassen

### Holzmasche

Eine Modelliermasse aus Sägespänen und Tapetenkleister. Auf 1 l Sägespäne kommen 1/2 l Wasser und 31 g Tapetenkleister (Methylcellulose). Den Tapetenkleister 20 Minuten quellen lassen, dann zusammen mit den Sägespänen zu einem Teig verkneten. Die Masse lässt sich leicht formen und verarbeiten. Durch den hohen Wasseranteil dauert die Trocknung bei dicken Bauteilen mehrere Tage. Hier kann es zu Schimmelbildung kommen. Dies könnte durch die Zugabe von Sumpfkalk oder Citronensäure verzögert werden. Generell wäre es bei tragenden Bauteilen sinnvoll einen etwas stärkeren Kleister zu verwenden. 

Die getrockneten Bauteile sind sehr leicht und verhalten sich ähnlich wie Holz. Sie können gesägt, gebohrt und geschliffen werden. Eine Verstärkung mit grobem Gewebe scheint möglich, aber auch hierfür sollte ein stärkerer Kleister genommen werden (siehe Sojaprotein-Leim).

### Quellen

- Carl Breuer. 1907. *Kitte und Klebstoffe*. M. Jänecke. http://archive.org/details/kitteundklebsto00breugoog (January 4, 2020).


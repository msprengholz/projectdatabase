# Recipes for Photographic Chemicals

## Iron-(III)-Oxalate (Ferric Oxalate)

The iron salt ferric oxalate is used as a light sensitive solution in alternative photography / printing. Under UV light it turns into ferrous oxalate (iron-(II)-oxalate). There are several methods for obtaining this compound, although most seem quite complicated. Producing it directly from iron oxides (described in 10.1021/acssuschemeng.0c03593) and oxalic acid seems very easy to do. I have not found it described elsewhere.

Using steel wool as the iron source yields ferrous oxalate and is therefore not useful.

As iron oxide source ($`{Fe}_2 O_3`$) paint pigments (red and yellow) can be used.

Reaction using yellow pigment (Geothite, $` \alpha FeO(OH) `$ or $`Fe_2O_3 \cdot H_2O`$) and oxalic acid dihydrate:

$`Fe_2O_3\cdot H_2O_{(s)} + 3C_2H_2O_4 \cdot 2H_2O_{(aq)} \rarr Fe_2{(C_2 O_4)}_{3_{(aq)}} + 10H_2O`$

For a resulting 20% concentrated solution of ferric oxalate the following masses are needed:

| Substance | mass in g | molar mass M in g/mol | moles n of substance | 
| ------ | ------ | ------ | ------ |
| **in**  |
| $`Fe_2O_3\cdot H_2O`$| 4.73 | 88.85 | 0.053 |
| $`3C_2H_2O_4 \cdot 2H_2O`$| 20.13 | 126.07 | 0.1597 |
| $`H_2O`$| 90.41 | 18.02 | 0.53 |
| **out**  |
| $`Fe_2{(C_2 O_4)}_{3}`$| 20 | 375.747 | 0.053 |
| $`H_2O`$| 9.59 | 18.02 | 0.53 |



The reaction is very slow (several days, the iron oxide does not keep in solution and needs to be stirred) at room temperature. The above mentioned paper recommends a temperature of around 80 to 90°C for dissolving iron oxide in oxalic acid.

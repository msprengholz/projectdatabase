# Soy Protein Glue

## Idea

Make a glue (primarily for wood) out of soy protein that is relatively water proof and uses only plant based materials.

## Previous Experiments

I tried using recipes for casein glues and substituting the casein with soy protein isolate (SPI).
I used lime putty (Ca(OH)2) to produce the alkaline medium in which the proteins denature. The best formula I found contained 5 parts SPI (with citric acid), 30 parts water and 1 part lime putty. This glue was not very strong and very soluble in water. Adjusting the ph to 4.4 (isoelectric point of the soy protein) did not increase water resistance and yielded a much weaker glue. In hindsight I would say that probably not enough lime putty was used. Problematic is the high sweilling of SPI in alkaline water. This yields a gel when the water content is lowered. This is not a problem when the ph is adjusted to be acidic.

## A new beginning

I recently read a new recipe over at https://simplifier.neocities.org/caseinglue.html. This inspired me try this with SPI instead of casein again.

### First mixture

- 57.7 g of H2O, 10 g of SPI, 22.2 g of 9% Ammonia Water (2 g of pure NH3)
  - this is double the water than recommended in the source, but otherwise the glue is so thick as not to mix at all
  - the glue thins under shear load (stirring) and can thus be applied more easily
    - thus the water content could possibly be further reduced
  - a test piece of pine was used to determine the strenth, both surface where planed / sanded smooth 
    - the glue was applied in a thick layer, both sides were connected and left to dry a bit to reduce water content (else too much glue gets squeezed out when pressure is applied and the bond weakens considerably)
    - the pieces are applied with some pressure (around 0.02 MPa instead of the recommended 1 MPa (US FPL), although smoother / better fitting surfaces need less pressure!)
    - when breaking the joint, only around 40 % is wood failure, the rest splits at the joint
      - this is still better than previous results and therefore encouraging!

### Second mixture

- try with less water

## Sources

- https://simplifier.neocities.org/caseinglue.html
- CASEIN GLUES: THEIR MANUFACTURE, PREPARATION, AND APPLICATION (US Forest 	Products Laboratory)
- Sun, Susan, Donghai Wang, Zhikai Zhong, and Guang Yang. 2008. “Adhesives from Modified Soy Protein.” https://patents.google.com/patent/US7416598B2/en?q=soy&q=oil&q=adhesive&oq=soy+oil+adhesive (US Pat. US 7416,598 B2) (October 13, 2019).
- Umemura, Kenji, Tomohide Ueda, Sasa Sofyan Munawar, and Shuichi Kawai. 2012. “Application of Citric Acid as Natural Adhesive for Wood.” *Journal of Applied Polymer Science* 123(4): 1991–96.

-----------
Firsts test, german:

# Sojaprotein-Leim

### Idee

Eine Alternative zu PU-Leim für Holzbauanwendungen. Als Vorlage dient Casein-Leim, der durch die Denaturierung von Milchprotein im alkalischen Medium seine klebenden Eigenschaften entwickelt. Das Milcheiweiß wird durch Sojaprotein ersetzt. Dieses ist in unterschiedlichen Formen zu bekommen. Als Sojamehl (50% Protein, hoher Kohlenhydratanteil) oder Sojaprotein-Isolat (SPI, >90% Protein). Denkbar wären auch andere Eiweißquellen (z.B. Erbsen). SPI bietet durch weniger Kohlenhydrate deutliche bessere Eigenschaften im Bezug auf Wasserbeständigkeit und geringere Anfälligkeit gegenüber mikrobieller Zersetzung. Ziel ist es einen Leim zu erstellen, der leicht herzustellen und zu verarbeiten sowie zu einem gewissen Grad wasserbeständig ist.

### Rezeptur

Ein Proteinleim benötigt an sich nur zwei Dinge: Eiweiß (in Pulverform) und eine basische (alkalische) Umgebung. Hierfür wird oftmals Sumpfkalkmilch verwendet. Zur Verbesserung der Wasserbeständigkeit werden oft auch weitere Zusätze wie Kaliwasserglas verwendet.

| Eiweißquelle  | alkalisches Medium                         | Zusätze                       |
| ------------- | ------------------------------------------ | ----------------------------- |
| SPI, Sojamehl | Sumpfkalkmilch (Ca(OH)2), Ätznatron (NaOH) | Kaliwasserglas, Zitronensäure |

Alle Zutaten werden einem Mörser vermengt, da das Sojaprotein zu klumpen beginnt bei Kontakt mit dem alkalischen Medium. Ein einfaches verrühren ist somit nicht möglich. Es empfiehlt sich zuerst Wasser, Sumpfkalk und Zusätze glatt zu verrühren und zuletzt das Eiweiß hinzu zugeben um eine gleichmäßige Verteilung zu erreichen. Das typische 'Knallen' bei schnellem Rühren ist auch hier wie beim Casein-Leim zu vernehmen. Die besten Ergebnisse ließen sich mit folgenden Mengen erreichen:

- Massenverhältnis von SPI zu Wasser zu Sumpfkalk: 5:30:1

SPi bindet generell deutlich mehr Wasser, weshalb bestehende Casein-Rezepte daraufhin angepasst werden müssen (Casein zu Wasser: 1:(1.5 bis 2.5)). Für dieses Rezept stellte sich das größte Holzversagen bei den Kerbschlagversuchen mit Kiefernholz ein. Allerdings versagte immer noch ca. 75% der Klebefläche anstatt des Holzes. Es handelte sich dabei um trockene Proben. Bei Nässe fielen die Leimfugen auseinander.

Um die Wasserbeständigkeit zu erhöhen, wurde versucht den Leim in ein saures Milieu zu überführen. Bei einem pH-Wert von rund 4,4 hat das Sojaprotein seinen isoelektrischen Punkt erreicht. Dort sollte seine Wasseraffinität am geringsten sein. Das vorangegangene Rezept wurde mittels Citronensäure auf den diesen pH-Wert gebracht. Das Protein verklumpt nun nicht mehr. Generell scheint so keine Denaturierung stattzufinden. Die erreichten Ergebnisse waren allesamt schlechter. In der Quelle wurde Klebeeigenschaften ähnlich zu Phenolharzen bescheinigt, allerdings wurde dort auch unter hohem Druck und hoher Temperatur (>160 °C) verklebt. Bei solchen Bedingungen eignet sich Citronensäure allein schon als guter, wasserbeständiger Holzkleber.

### Verbesserung

Diese Ergebnisse sind nicht ausreichend, da der Leim so nicht anwendbar ist. Die Zugabe von Wasserglas wurde nicht weiter untersucht. Außerdem sollte auch mit der Menge an Sumpfkalk experimentiert werden.

### Quellen

- CASEIN GLUES: THEIR MANUFACTURE, PREPARATION, AND APPLICATION (US Forest 	Products Laboratory)
- Sun, Susan, Donghai Wang, Zhikai Zhong, and Guang Yang. 2008. “Adhesives from Modified Soy Protein.” https://patents.google.com/patent/US7416598B2/en?q=soy&q=oil&q=adhesive&oq=soy+oil+adhesive (US Pat. US 7416,598 B2) (October 13, 2019).
- Umemura, Kenji, Tomohide Ueda, Sasa Sofyan Munawar, and Shuichi Kawai. 2012. “Application of Citric Acid as Natural Adhesive for Wood.” *Journal of Applied Polymer Science* 123(4): 1991–96.

